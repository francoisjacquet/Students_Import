Students Import Premium module
==============================

![screenshot](https://www.rosariosis.org/wp-content/uploads/2021/08/rosariosis_students_import_premium_en_2021.png)

FEATURES
--------

- Import Addresses & Contacts (Father, Mother, Guardian...) and their information.
- Import Addresses & People custom fields.
- Create Food Service Accounts (and assign a Barcode).
- Generate generic Username & Password so Students can login.
- Update Existing Students info (identify students based on their ID, Username or Name).
- Change the default value ("Y") recognized as checked state: for example, "Yes" (for _Checkbox_ custom fields).
- Create Student in Moodle (if Username and Email Address are set).

[![additional-screenshot](https://www.rosariosis.org/wp-content/uploads/2021/08/rosariosis_students_import_premium_en_2021.png)](https://www.rosariosis.org/wp-content/uploads/2021/08/rosariosis_students_import_premium_en_2021.png) [![additional-screenshot](https://www.rosariosis.org/wp-content/uploads/2016/07/rosariosis_students_import_premium_screenshot_2-300x187.png)](https://www.rosariosis.org/wp-content/uploads/2016/07/rosariosis_students_import_premium_screenshot_2.png)

### [➭ Switch to Premium](https://www.rosariosis.org/modules/students-import/#premium-module)
