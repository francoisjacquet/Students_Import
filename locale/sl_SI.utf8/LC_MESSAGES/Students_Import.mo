��          �      ,      �     �     �     �  X   �       4   .  D   c     �     �     �  �   �  
   d     o  #   �     �     �  �  �     �  	   �     �  i   �     [  3   x  8   �     �     �     
  �   #     �     �  #   �                	                                                 
                        %d notifications sent. %s rows %s students were imported. Are you absolutely ready to import students? Make sure you have backed up your database! Cannot open file. IDs are automatically generated if you select "N/A". If the date is left empty, students will not be enrolled (inactive). Import Students Import first row No names were found. Notifications are sent if Username, Password, Email Address are set and Attendance Start Date this School Year is on or before today. Reset form Select CSV or Excel file Send email notification to Students Stop Students Import Project-Id-Version: Students Import Premium Module for RosarioSIS.
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2024-04-22 18:44+0200
Last-Translator: François Jacquet <info@rosariosis.org>
Language-Team: RosarioSIS <info@rosariosis.org>
Language: fr_FR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Poedit-KeywordsList: ;dgettext:2
X-Poedit-Basepath: ../../..
X-Generator: Poedit 3.2.2
X-Poedit-SearchPath-0: .
 %d poslanih obvestil. %s vrstic %s dijakov je bilo uvoženih. Ali ste povsem pripravljeni na uvoz študentov? Prepričajte se, da ste varnostno kopirali bazo podatkov! Datoteke ni mogoče odpreti. ID-ji se ustvarijo samodejno, če izberete »N/A«. Če je datum prazen, dijaki ne bodo vpisani (neaktivni). Importer Élèves Uvozi prvo vrstico Imena niso bila najdena. Obvestila se pošljejo, če so nastavljeni uporabniško ime, geslo, e-poštni naslov in začetni datum prisotnosti v tem šolskem letu na današnji dan ali prej. Ponastavi obrazec Izberite datoteko CSV ali Excel Pošlji e-poštno obvestilo dijakom Stop Import d'Élèves 