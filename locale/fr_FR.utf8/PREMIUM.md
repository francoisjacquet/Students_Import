Module Import d’élèves Premium
==============================

![screenshot](https://www.rosariosis.org/wp-content/uploads/2021/08/rosariosis_students_import_premium_fr_2021.png)

CARACTÉRISTIQUES
----------------

- Import des adresses et contacts (père, mère, tuteur…) et leurs informations.
- Import des champs configurés des Adresses et contacts.
- Créer les comptes de cantine (et assigner un code barre).
- Génération de nom d’utilisateur et mot de passe génériques afin que les élèves puissent se connecter.
- Mise à jour des infos des élèves existants (identifier les élèves par leur ID, nom d’utilisateur ou nom).
- Changer la valeur par défaut (« Y ») reconnue comme état coché : par exemple, « Oui » (pour vos champs de type _Case à cocher_).
- Créer l’élève dans Moodle (si le nom d’utilisateur et l’adresse email sont définis).

[![additional-screenshot](https://www.rosariosis.org/wp-content/uploads/2021/08/rosariosis_students_import_premium_fr_2021.png)](https://www.rosariosis.org/wp-content/uploads/2021/08/rosariosis_students_import_premium_fr_2021.png) [![additional-screenshot](https://www.rosariosis.org/wp-content/uploads/2016/07/rosariosis_students_import_premium_fr_screenshot_2-300x187.png)](https://www.rosariosis.org/wp-content/uploads/2016/07/rosariosis_students_import_premium_fr_screenshot_2.png)

### [➭ Passer à Premium](https://www.rosariosis.org/fr/modules/students-import/#premium-module)
