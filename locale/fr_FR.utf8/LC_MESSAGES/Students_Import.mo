��          �      <      �     �     �     �  X   �     D  G   V  4   �  D   �          (     9  �   N  
   �     �  #   �          !  �  1       	   7      A  d   b  $   �  [   �  C   H  O   �     �     �       �   +     �  "   	  +   )	     U	     Z	                                          	                                 
                          %d notifications sent. %s rows %s students were imported. Are you absolutely ready to import students? Make sure you have backed up your database! Cannot open file. Excel error reading file %s: %s. Please convert the file to CSV format. IDs are automatically generated if you select "N/A". If the date is left empty, students will not be enrolled (inactive). Import Students Import first row No names were found. Notifications are sent if Username, Password, Email Address are set and Attendance Start Date this School Year is on or before today. Reset form Select CSV or Excel file Send email notification to Students Stop Students Import Project-Id-Version: Students Import Module for RosarioSIS.
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2024-04-22 18:44+0200
Last-Translator: François Jacquet <info@rosariosis.org>
Language-Team: RosarioSIS <info@rosariosis.org>
Language: fr_FR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Poedit-KeywordsList: ;dgettext:2
X-Poedit-Basepath: ../../..
X-Generator: Poedit 3.2.2
X-Poedit-SearchPath-0: .
 %d notifications envoyées. %s lignes %s élèves ont été importés. Êtes-vous prêt pour importer les élèves ? Assurez-vous d'avoir sauvegardé la base de données ! Le fichier ne peut pas être ouvert. Erreur Excel à la lecture du fichier %s : %s. Merci de convertir le fichier au format CSV. Les ID sont automatiquement générés si vous sélectionnez "N/D". Si vous n'indiquez pas de date, les élèves ne seront pas inscrits (inactifs). Importer les élèves Importer la primière ligne Aucun nom n'a été trouvé. Les notifications sont envoyées si le nom d'utilisateur, le mot de passe, l'adresse email sont définis et si la date de début de présence cette année scolaire est aujourd'hui ou avant. Réinitialiser le formulaire Sélectionner fichier CSV ou Excel Envoyer notification par email aux élèves Stop Import d'élèves 