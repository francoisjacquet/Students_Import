��          �      <      �     �     �     �  X   �     D  G   V  4   �  D   �          (     9  �   N  
   �     �  #   �          !  �  1       
   8  #   C  b   g     �  U   �  :   >  N   y     �     �     �  �        �     �  0   �     !	     &	                                          	                                 
                          %d notifications sent. %s rows %s students were imported. Are you absolutely ready to import students? Make sure you have backed up your database! Cannot open file. Excel error reading file %s: %s. Please convert the file to CSV format. IDs are automatically generated if you select "N/A". If the date is left empty, students will not be enrolled (inactive). Import Students Import first row No names were found. Notifications are sent if Username, Password, Email Address are set and Attendance Start Date this School Year is on or before today. Reset form Select CSV or Excel file Send email notification to Students Stop Students Import Project-Id-Version: Students Import Module for RosarioSIS.
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2024-04-22 18:43+0200
Last-Translator: François Jacquet <info@rosariosis.org>
Language-Team: RosarioSIS <info@rosariosis.org>
Language: es_ES
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-KeywordsList: ;dgettext:2
X-Poedit-Basepath: ../../..
X-Generator: Poedit 3.2.2
X-Poedit-SearchPath-0: .
 %d notificaciones enviadas. %s líneas %s estudiantes han sido importados. Está listo para importar los estudiantes? Asegurese de haber hecho un backup de la base de datos! El archivo no se puede abrir. Error Excel leyendo el archivo %s: %s. Por favor convertir el archivo en formato CSV. Los IDs son automaticamente generados si selecciona "N/A". Si deja la fecha vacía, los estudiantes no estarán matriculados (inactivos). Importar Estudiantes Importar la primera línea No se encontró ningún nombre. Las notificaciones están enviadas si el Nombre de Usuario, la Contraseña, el Email están definidos y el Primer Día de Clase del Año Escolar es hoy o antes. Restablecer formulario Seleccionar archivo CSV o Excel Enviar notificación por email a los Estudiantes Pare Importación de Estudiantes 