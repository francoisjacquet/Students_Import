Módulo Importación de Estudiantes Premium
=========================================

![screenshot](https://www.rosariosis.org/wp-content/uploads/2021/08/rosariosis_students_import_premium_es_2021.png)

CARACTERÍSTICAS
---------------

- Importar Direcciones y Contactos (Padre, Madre, Tutor...) y su información.
- Importar campos configurados de Dirección y Contactos.
- Crear las cuentas del servicio de comida (y asignar un código de barras).
- Generar Nombres de Usuarios y Contraseñas genéricas para permitir el acceso de los estudiantes.
- Actualizar las informaciones de Estudiantes Existentes (identificar los estudiantes basado en el ID, el Nombre de Usuario o el Nombre).
- Cambiar el valor por defecto ("Y") reconocido para marcar las casillas: por ejemplo: "Sí" (para sus campos de tipo *Casilla*).
- Crear el Estudiante en Moodle (si el Nombre de Usuario y el Email están definidos).

[![additional-screenshot](https://www.rosariosis.org/wp-content/uploads/2021/08/rosariosis_students_import_premium_es_2021.png)](https://www.rosariosis.org/wp-content/uploads/2021/08/rosariosis_students_import_premium_es_2021.png) [![additional-screenshot](https://www.rosariosis.org/wp-content/uploads/2016/07/rosariosis_students_import_premium_es_screenshot_2-300x187.png)](https://www.rosariosis.org/wp-content/uploads/2016/07/rosariosis_students_import_premium_es_screenshot_2.png)

### [➭ Cambiar a Premium](https://www.rosariosis.org/es/modules/students-import/#premium-module)
