��          �      ,      �     �     �     �  X   �       4   .  D   c     �     �     �  �   �  
   d     o  #   �     �     �  �  �     r  	   �     �  o   �  #   $  ;   H  O   �     �     �       �        �     �  *        1     7     	                                                 
                        %d notifications sent. %s rows %s students were imported. Are you absolutely ready to import students? Make sure you have backed up your database! Cannot open file. IDs are automatically generated if you select "N/A". If the date is left empty, students will not be enrolled (inactive). Import Students Import first row No names were found. Notifications are sent if Username, Password, Email Address are set and Attendance Start Date this School Year is on or before today. Reset form Select CSV or Excel file Send email notification to Students Stop Students Import Project-Id-Version: Students Import Module for RosarioSIS.
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2024-04-22 18:44+0200
Last-Translator: Emerson Barros
Language-Team: 
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Poedit-KeywordsList: ;dgettext:2
X-Poedit-Basepath: ../../..
X-Generator: Poedit 3.2.2
X-Poedit-SearchPath-0: .
 %d notificações enviadas. %s linhas %s alunos foram importados. Você está absolutamente pronto para importar alunos? Certifique-se de ter feito backup de seu banco de dados! Não foi possível abrir o arquivo. IDs são gerados automaticamente se você selecionar "N/A". Se a data for deixada em branco, os alunos não serão matriculados (inativos). Importar Estudiantes Importar primeira linha Nenhum nome foi encontrado. As notificações são enviadas se o nome de usuário, a senha, o endereço de e-mail estiverem definidos e a data de início do comparecimento deste ano letivo for hoje ou antes. Redefinir formulário Selecione arquivo CSV ou Excel Enviar notificação por e-mail aos alunos Parar Importación de Estudiantes 